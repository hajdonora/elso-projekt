import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("test")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Test {

    @GET
    @Path("/{nev}")
    public Response testMethod(@PathParam("nev") String nev)
            
    {
       return Response.ok("szia "+nev).build();
    }

    @POST
    @Path("/post")
    public Response testMethod2(@QueryParam("eletkor") String eletkor )
    {

        return Response.ok(  "eletkor " + eletkor).build();
    }

}
